;; don't override prefix C-x, C-c, C-h
;; don't mess with screen's C-a
;; try not to bind <up> .. <right> key, in order to be compatible in different keyboard and system, except built-in ones.
;; try to use default key binding, unless
;; 1. you feel extremely uncomfortable about it
;; 2. default key is redundant

;; common redundant key
;; C-f, C-b, C-n, C-p, C-d
;; M-u

;; my key
;; C-d: replace


;;;; basic preference
;; prevent cusor from jumping back
(show-paren-mode 1)
(setq blink-matching-delay 0.05)

;; disable auto-save and auto-backup
(setq auto-save-default nil)
(setq make-backup-files nil)

;; show line number
(global-linum-mode t)
(set-default 'truncate-lines nil)
(setq scroll-step 20)

;; append an indent to newline
(global-set-key (kbd "RET") 'newline-and-indent)

;; automatically complete brackets
;(electric-pair-mode +1)


;; set indent
(setq standard-indent 2)

;; show line-number in the mode line
(line-number-mode 1)

;; show column-number in the mode line
;(column-number-mode 1)

(defalias 'yes-or-no-p 'y-or-n-p)

(setq search-whitespace-regexp nil)


(require 'uniquify)
(setq
 uniquify-buffer-name-style 'post-forward
 uniquify-separator ":")




;;;; key preference
;;(global-set-key (kbd "C-l") 'kill-whole-line)
(global-set-key "\033[1;5D" 'backward-word)
(global-set-key "\033[1;5C" 'forward-word)
(global-set-key "\033[1;5A" 'backward-paragraph)
(global-set-key "\033[1;5B" 'forward-paragraph)

(global-set-key (kbd "M-u") 'undo)


;; search/replace
(global-set-key (kbd "M-r") 'isearch-backward-regexp)
(global-set-key (kbd "M-s") 'isearch-forward-regexp)

(global-set-key (kbd "C-t") 'query-replace)
(global-set-key (kbd "M-t") 'query-replace-regexp)


;; duplicate line
(global-set-key "\C-p" "\C- \C-e\M-w")

;(global-set-key (kbd "M-i") 'pop-tag-mark)
(global-set-key (kbd "M-i") (kbd "M-*"))


(defun copy-line ()
  (interactive)
  (kill-line)
  (yank))
;;(global-set-key (kbd "M-l") 'copy-line)

(global-set-key (kbd "C-x <up>") 'windmove-up)
(global-set-key (kbd "C-x <down>") 'windmove-down)
(global-set-key (kbd "C-x <right>") 'windmove-right)
(global-set-key (kbd "C-x <left>") 'windmove-left)




;; hide GNU Emacs window at startup
;;;; don't know what t means
(setq inhibit-startup-screen t)





(require 'package)
; add MELPA to repository list
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
; initialize package.el
(package-initialize)

(require 'auto-complete)
; do default config for auto-complete
(require 'auto-complete-config)
(ac-config-default)


(add-hook 'c-mode-common-hook
	  (lambda ()
	    (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
	      (ggtags-mode 1))))


(add-hook 'python-mode-hook 'anaconda-mode)
(add-hook 'python-mode-hook 'autopair-mode)
(add-hook 'c-mode-common-hook 'autopair-mode)

;; etags
;;(setq tags-table-list '("~/.local/TAGS"))

;;(setq tags-table-list '("~/projects/jit-comp/TAGS"))
(setq tags-table-list '("./TAGS" "~/projects/jit-comp/TAGS"))
;;(setq tags-file-name "~/.local/TAGS")


;; CEDET

;; will ask you "Really quit?", very annoying
;;(setq confirm-kill-emacs 'yes-or-no-p)


;; test

